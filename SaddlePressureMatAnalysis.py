# -*- coding: utf-8 -*-
"""
Created on Thu May 28 18:00:50 2020

@author: jfrank
"""


import pandas as pd
import numpy as np
import matplotlib.pyplot as plt
import seaborn as sns
import os


get_ipython().run_line_magic('matplotlib', 'inline')

sns.set_style('whitegrid')

df = pd.read_excel(r"C:\Users\jfrank\OneDrive - Specialized Bicycle Components\Projects\Saddles\Custom Saddles\2020059\New8wkg.xlsx",'Frames')
df.info()





df.set_index('Frame Id',inplace = True)

dfT = df.T

#Calc means
hc = dfT['Horizontal Center (cm)'].astype(float).mean()
print(hc)
vc = dfT['Vertical Center (cm)'].astype(float).mean()
print(vc)
avg = dfT['Average (psi)'].astype(float).mean()
avg = avg * 6.89
print(avg)
F = dfT['Force (N)'].astype(float).mean()
F_std = dfT['Force (N)'].astype(float).std()
print(F)

#Calc Std
hc_std = dfT['Horizontal Center (cm)'].astype(float).std()
print('\n', hc_std)
vc_std = dfT['Vertical Center (cm)'].astype(float).std()
print(vc_std)
avg_std = dfT['Average (psi)'].astype(float).std()
avg_std = avg_std * 6.89
print(avg_std)
F_std = dfT['Force (N)'].astype(float).std()
print(F_std)
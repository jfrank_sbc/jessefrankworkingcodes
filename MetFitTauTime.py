                     # -*- coding: utf-8 -*-
"""
Created on Tue Apr 28 15:44:39 2020

@author: jfrank
"""


import pandas as pd
import numpy as np
import matplotlib.pyplot as plt
import seaborn as sns
import glob
from io import StringIO
import scipy.special as scp
from scipy.optimize import curve_fit 
import os
import datetime as dt
from datetime import datetime
#%matplotlib inline

sns.set_style("whitegrid", {'grid.linestyle': '--'})
pd.set_option('display.float_format', lambda x: '%.8f' % x)


df = pd.read_excel(r"C:\Users\jfrank\OneDrive - Specialized Bicycle Components\Projects\MetFit2.0\20200507 Jesse Frank (Actual75%).xlsx")
#df = pd.read_excel(r"C:\Users\jfrank\OneDrive - Specialized Bicycle Components\Projects\MetFit2.0\Pnoe_20200430_1642-Jason_Williams.xlsx")
#df = pd.read_excel(r"C:\Users\jfrank\OneDrive - Specialized Bicycle Components\Athlete Testing\Labeled Met Fit Files\Analyzed\20200312 Jared Scott.xlsx")


## Determine if file came from PNOE OR COSMED. Handle columm names accordingly
## If time column is labeled as 'T(sec)' it comes from PNOE.
if any(df.columns.values == 'T(sec)'):
    print('PNOE data')
    d_source = 'PNOE'
    start_indices_labels = df.Marker.loc[df.Marker.notnull()]
    #start_indices_labels = df.Stages.loc[df.Stages.notnull()]
    start_indices = start_indices_labels.index

    #df_VO2 = df.VO2.loc[start_indices[5]:start_indices[6]]
    df_stable= df.loc[start_indices[0]:start_indices[3]]

    df_stable['VO2(ml/min)'].plot()
    t_stable = df_stable['T(sec)']
    y = df_stable['VO2(ml/min)'] #y data for Tau Time later
    y_HR = df_stable['HR(bpm)']

    
else: # Data comes from Cosmed
    print('Cosmed Data')
    d_source = 'Cosmed'
    t = df.t
    for index, i in enumerate(t[2:-1]): #Convert time column into seconds from start of test
        t[(index + 2)] = i.hour *3600 + i.minute*60 + i.second
        # t[(index +2)] = float(t[index + 2])

    start_indices_labels = df.Marker.loc[df.Marker.notnull()]
    #start_indices_labels = df.Stages.loc[df.Stages.notnull()]
    start_indices = start_indices_labels.index

    #df_VO2 = df.VO2.loc[start_indices[5]:start_indices[6]]
    df_stable= df.loc[start_indices[2]:start_indices[3]]

    df_stable['VO2'][2:].plot()
    t_stable = df_stable['t'][2:]
    y = df_stable['VO2'][2:]
    y_HR= df_stable['HR'][2:]
    
#### Do this for all data, PNOE or Cosmed    

for index, j in enumerate(t_stable): #Determine time differnce from beginning of stage to data point. This will be x-axis
    if index == 0:
        t1 = j
        t_stable[t_stable.index[index]] = 0
    else:
        t2 = j
        t_stable[t_stable.index[index]]=t2-t1
    

t_stable.plot()
x = t_stable.astype(float)


#--------------------------------------------------
# T = T_i + alpha(1-exp(-t/tau))
#--------------------------------------------------

T_i = y.iloc[0]

def test3(x, alpha, tau):
    return T_i + alpha*(1-np.exp(-x/tau))

## Tau Time VO2

param3, param_cov3 = curve_fit(test3, x, y,[10, 1000.0]) 
ans3 = T_i + param3[0]*(1-np.exp(-x/param3[1]))

print("VO2 Coefficients:") 
print('VO2 alpha: ' + str(param3[0]))  
print('VO2 time constant, tau: ' + str(param3[1]))  

plt.figure (figsize = (12, 6))
if d_source == 'Cosmed':
    plt.plot(x, df_stable['VO2'][2:], 'o', color ='r', label ="data", alpha=0.2) 
elif d_source == 'PNOE' :
    plt.plot(x, df_stable['VO2(ml/min)'], 'o', color ='r', label ="data", alpha=0.2) 
else:
    print('Check Data Source')
    exit()
    
plt.plot(x, ans3, '--', color ='b', label ="VO2 curve_fit data", linewidth=3) 
plt.legend() 
plt.show() 

## Tau Time HR
T_iHR = y_HR.iloc[0]

def test31(x, alpha, tau):
    return T_iHR + alpha*(1-np.exp(-x/tau))

param3HR, param_cov3HR = curve_fit(test31, x, y_HR,[5, 80]) 
ans3HR = T_iHR + param3HR[0]*(1-np.exp(-x/param3HR[1]))

print("HR Coefficients:") 
print('HR alpha: ' + str(param3HR[0]))  
print('HR time constant, tau: ' + str(param3HR[1]))  

plt.figure (figsize = (12, 6))
if d_source == 'Cosmed':
    plt.plot(x, df_stable['HR'][2:], 'o', color ='r', label ="data", alpha=0.2) 
elif d_source == 'PNOE' :
    plt.plot(x, df_stable['HR(bpm)'], 'o', color ='r', label ="data", alpha=0.2) 
else:
    print('Check Data Source')
    exit()
    
plt.plot(x, ans3HR, '--', color ='b', label ="HR curve_fit data", linewidth=3) 
plt.legend() 
plt.show() 
function [met_summary,PmetDiffAero0, PmetDiffRace, PmechDiff_MetFit, PmechDiff_Race, testPwr,racePwr, mean_gme ] = MetFit2Analysis( mean_diff, aerobars,basebars, fname, stage, sandwiches, diffs, std_diff);
% This function does all the calculations and intermediary steps to scale
% the met fit results to aero power. The outputs are:

%PmetDiffAero: The metabolic power difference between Aero0 and the
%subsequent aero positions at the met fit effort

%PmetDiffRace: The metabolic power difference between Aero0 and the
%subsequent aero positions at race effort

%PmechDiff_MetFit: Mechanical Power difference between Aero0 and the
%subsequent aero positions at met fit effort.

%PmechDiff_Race: Mechanical Power difference between Aero0 and the
%subsequent aero positions at race effort.


%Get test wattage from user
testPwr = inputdlg('Enter the test wattage');
testPwr = str2double(testPwr);

%Get ideal Race Power
racePwr = inputdlg('Enter the riders goal race wattage');
racePwr = str2double(racePwr);

%Determine Gross Mechanical Efficiency (GME) for each stage
gme = testPwr./aerobars;

%Average the GME for each aero position, since each aero position was done
%2x.

for jj = 1:3
    mean_gme(1) = mean(testPwr./basebars);
    mean_gme(jj+1) = mean([gme(jj),gme(length(gme)-(jj-1))]);
    mean_gme(jj+1) = mean([gme(jj),gme(length(gme)-(jj-1))]);
    mean_gme(jj+1) = mean([gme(jj),gme(length(gme)-(jj-1))]);
end
% for jj = 1:3
%     mean_gme(jj,1) = mean([gme(jj,1),gme(length(gme)-jj,1)]);
%     mean_gme(jj,2) = mean([gme(jj,1),gme(length(gme)-jj,2)]);
%     mean_gme(jj,3) = mean([gme(jj,1),gme(length(gme)-jj,3)]);
% end


%Caculate outputs
for i = 2:length(mean_diff)+1
    if i == 2 %Calcualte for basebarvcondition
        PmetDiffAero0(i-1) = -mean_diff(i-1); %mean_diff measurement gives W in terms aero diff from basebars. We want basebar difference from aerobar. So flip the sign
        PmetStdDiffAero0(i-1) = std_diff(i-1);
        PmetDiffRace(i-1) = (racePwr/testPwr).*PmetDiffAero0(i-1);
        PmetStdDiffRace(i-1) = (racePwr/testPwr).*PmetStdDiffAero0(i-1);
        PmechDiff_MetFit(i-1) = PmetDiffAero0(i-1).* mean_gme(i-1);
        PmechDiff_Race(i-1) = PmetDiffRace(i-1).* mean_gme(i-1);
        PmechStdDiff_Race(i-1) = PmetStdDiffRace(i-1).* mean_gme(i-1);
    else
        PmetDiffAero0(i) = mean_diff(i-1)-mean_diff(1); %mean_diff measurement gives W in terms of diff from basebars. Want diff from Aero0, so subtract diff from basebars
        PmetStdDiffAero0(i) = std_diff(i-1);
        PmetDiffRace(i) = (racePwr/testPwr).*PmetDiffAero0(i);
        PmetStdDiffRace(i) = (racePwr/testPwr).*PmetStdDiffAero0(i);
        PmechDiff_MetFit(i) = PmetDiffAero0(i).* mean_gme(i);
        PmechDiff_Race(i) = PmetDiffRace(i).* mean_gme(i);
        PmechStdDiff_Race(i) = PmetStdDiffRace(i).* mean_gme(i);
    end

end

% for i = 2:length(mean_diff)
% PmetDiffAero0(i,:) = mean_diff(i,:)-mean_diff(1,:);
% PmetDiffRace(i,:) = (racePwr/testPwr).*PmetDiffAero0(i,:);
% PmechDiff_MetFit(i,:) = PmetDiffAero0(i,:).* mean_gme(i,:);
% PmechDiff_Race(i,:) = PmetDiffRace(i,:).* mean_gme(i,:);
% 
% end

%Graph mech power differences at race effort.

pos = inputdlg('Please enter the trial conditions in the order they appear, separated by a comma. For ex. Basebars,Aero0,+2 cm, -2 cm');
pos = split(pos,",");
figure
bar([1,2,3,4],PmechDiff_Race');
hold on
er  = errorbar([1,2,3,4],PmechDiff_Race',PmechStdDiff_Race',PmechStdDiff_Race');
er.Color = [0 0 0];                            
er.LineStyle = 'none'; 
title(sprintf('Mechanical Power Differences Compared to Aero0 at Target Race Power: %d W',racePwr));
set(gca, 'xticklabels',{pos{1},pos{2},pos{3},pos{4}});
xtickangle(45);
ylabel('Mechanical Power (W)');
xlabel('Stage Condition')
si = fname(1:length(fname)-5);
saveas(gcf, strcat(si,'_Graph.png'));

%% Create Data Table
Position = pos;
Met_Fit_Wattage = [testPwr; testPwr;testPwr;testPwr];
Target_Race_Wattage = [racePwr;racePwr;racePwr;racePwr];
Met_Power_Diff = [PmetDiffAero0'];
Met_Power_Diff_Race_Wattage = [PmetDiffRace'];
Gross_Mechanical_Efficiency = [mean_gme'.*100];
Mech_Power_Diff = [PmechDiff_MetFit'];
Mech_Power_Diff_Race_Wattage = [PmechDiff_Race'];

met_summary = table(Position,Met_Fit_Wattage,Target_Race_Wattage,Met_Power_Diff,Met_Power_Diff_Race_Wattage,Gross_Mechanical_Efficiency,Mech_Power_Diff,Mech_Power_Diff_Race_Wattage);
f = figure('Name','Metabolic Fit Results Summary','Position',[330 800 1020 200]);
t = uitable(f,'Data', table2cell(met_summary), 'Position',[10 10 1000 190]);
t.ColumnName={met_summary.Properties.VariableNames{:}};
t.RowName=[]; %removing default row numbering as in your uitable
exportgraphics(f,strcat(si,'_MetFitTable.png'));

%%Save Workspace
save(strcat(si,'.mat'))
end
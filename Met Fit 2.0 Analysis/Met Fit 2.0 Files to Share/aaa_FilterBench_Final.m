function [data, fname] = aaa_FilterBench_Final();

%%% 

% This is Marcel's code. Jesse added a few things. 

%fname = inputdlg('Enter file name of data to be analyzed. DO NOT INCLUDE the file extension.'); 
fname = uigetfile('*.xlsx');
[~, ~, raw] = xlsread(fname, 'Data');
%[~, ~, raw] = xlsread('20200918 Rocco Orlando (BW Pilot)', 'Data');
%%
data.t      = 24*3600*[raw{4:end,10}]';
data.VO2    = [raw{4:end,15}]';
data.EEm    = [raw{4:end,45}]'*69.78;

%extract marker
Marker = raw(4:end,37);
testStartID = find(cellfun(@ischar, Marker));
testTitle   = Marker(testStartID);
testTime    = data.t(testStartID);

% Resample to unique step size
fSampling = 1; %[Hz]

%leading signal Time must be unique
[data.t, uniqueIDs] = unique(data.t);
data.VO2 = data.VO2(uniqueIDs);
data.EEm = data.EEm(uniqueIDs); %All .EEm variables/structs were added by Jesse so we can have VO2 and EEm data ready to go.


data.t = round(data.t, 1); %otherwise "next" method cause delay on one sample

%% apply weighted Mov Mean filter

% calculate smoothed signal
%Marcel's initial code looking at previous 14 breaths means
nFilt = 15; %[amount of breath] to average
data.VO2_movmean         = movmean(data.VO2, [nFilt-1 0]);
data.EEm_movmean         = movmean(data.EEm, [nFilt-1 0]);

%Std
%data.VO2_movstd         = movstd(data.VO2, [nFilt-1 0]);
%data.EEm_movstd         = movstd(data.EEm, [nFilt-1 0]);
data.VO2_movstd         = movstd(data.VO2_movmean, [nFilt-1 0]);
data.EEm_movstd         = movstd(data.EEm_movmean, [nFilt-1 0]);


%add a trigger signal, where a new test was starting
data.idMarker = false(size(data.t));
data.idMarker( arrayfun(@(X) find(data.t>=X, 1, 'first'), testTime) ) = true;

%save to excel sheet.
%Adds the filtered data to the K5 excel output. That way all of the data is
%in one place. 
%writetable(struct2table(data),strcat(fname,'.xlsx'),'Sheet','Data','Range','CK3:CP5000')
writetable(struct2table(data),fname,'Sheet','Data','Range','CK3:CP5000')
%% plot stuff
% f = figure;
% ax = axes; hold on; grid on; grid minor
% 
% lw = 1.5;
% plot(ax, data.t, data.VO2_movmean, 'LineWidth', lw, 'DisplayName', '15 breath simple')
% plot(data.t(data.idMarker), data.VO2(data.idMarker), 'b', 'LineWidth', lw, 'DisplayName', 'TestTrigger', 'linestyle', 'none', 'marker', 'x', 'LineWidth', 3)
% 
% legend show
% xlabel 'time [s]'
% ylabel 'value [unit/min]'




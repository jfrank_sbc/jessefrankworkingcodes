function out = movmeanDiscont(signal, t, k)
%
% This function builds the moving average of signals that
% are samples with non-uniform sampling rate
%
% Input
%  signal  - raw signal
%  t       - time vector, same length as signal
%  k       - input similar to native function movmean. Can be scalar or 2xn using [kb kf] format
%
% Output 
%  out     - filtered signal
%

tStep = [t(1); diff(t)];
signal_Weighted = signal .* tStep;

out   = movmean(signal_Weighted, k) ./ movmean(tStep, k);

end %main function

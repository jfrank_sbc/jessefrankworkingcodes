%% This script combines the last two scripts in the Met Fit 2.0 post processing. This leads to one less button press.

[data, fname] = aaa_FilterBench_Final();
[sandwiches, diffs, stage, mean_diff, aerobars,basebars, fname, std_diff] = FilteredAveraging_Final(data,fname);
[met_summary, PmetDiffAero0, PmetDiffRace, PmechDiff_MetFit, PmechDiff_Race, testPwr,racePwr, mean_gme ] = MetFit2Analysis( mean_diff, aerobars,basebars, fname, stage, sandwiches, diffs,std_diff);
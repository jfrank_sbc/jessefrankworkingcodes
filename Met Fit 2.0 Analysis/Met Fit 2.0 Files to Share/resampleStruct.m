function sOut = resampleStruct (sIn, refChannel, stepSize, interpMethod, applyAntialiasing, fieldsOut)
%sOut = resampleStruct (sIn, refChannel, stepSize, interpMethod, applyAntialiasing, fieldsOut)
%
% Function to resample a data structure based on a refernce channel to a fix sampling rate.
%
% Inputs
%   sIn          - data struct to resample
%   refChannel   - field name of the reference data channel
%   stepSize     - scalar: output frequency of refChannel (equals 1/sampling rate)
%                  vector: will be used as refChannel
%
%  Optional Inputs
%   interpMethod      - interp1 method (default 'linear', e.g. for K5 we use 'next')
%   applyAntialiasing - true / false (default) apply aliasing filter to each data channel before resampling
%   fieldsOut         - cell array of output field names, default is all fields from sIn
%
% Output
%   sOut         - struct with fixes stepsize fs for refChannel and interpolated signals for all other

%ToDo1 - move back to SBC toolbox

if nargin<4 || isempty(interpMethod)
  interpMethod = 'linear';
end
if nargin<5 || isempty(applyAntialiasing)
  applyAntialiasing = false;
end
if nargin<6 || isempty(fieldsOut)
  fieldsOut = fieldnames(sIn);
end

if length(stepSize)==1
  %ToDo2 Ceil and Floor like this makes only sense when stepsize is 1 !!!
  sOut.(refChannel) = (ceil(sIn.(refChannel)(1)):(stepSize):floor(sIn.(refChannel)(end)))';
else
  sOut.(refChannel) = stepSize;
  stepSize = min(diff(stepSize)); %needed for filter
end
%prepare filter
if applyAntialiasing
  fInput      = round(mean(1./diff(sIn.(refChannel))));
  if fInput<2*(1/stepSize) %check if anti aliasing makes sense depending on ratio of input and output frequency
    applyAntialiasing = false;
    warning('resampleStruct without anti aliasing, because input frequence smaller than 2x output frequence');
  else
    filterorder = 30; %same as function "decimate", but I actually double the order due to filtfilt
    myFilter = fir1(filterorder, 0.999 * (1/stepSize) * 2 / fInput, 'low');  %apply 0.999 to allow reducing frequency to 50% (nycist 1)
  end
end

for iField = 1:length(fieldsOut)
  if ~strcmp(fieldsOut{iField}, refChannel) %skip reference channel, this signal is build already
    if isnumeric(sIn.(fieldsOut{iField}))
      %filter signal with anti aliasing filter
      if ~applyAntialiasing
        inSignal = sIn.(fieldsOut{iField});
      else
        inSignal = filtfilt(myFilter, 1, sIn.(fieldsOut{iField}));
      end   
      sOut.(fieldsOut{iField}) = ...
        interp1 (sIn.(refChannel), inSignal, sOut.(refChannel), interpMethod);
    else
      try %should work e.g. for datetime, but e.g. logicals need special handling
        sOut.(fieldsOut{iField}) = ...
          interp1 (sIn.(refChannel),  sIn.(fieldsOut{iField}), sOut.(refChannel), interpMethod);
      catch
        if islogical(sIn.(fieldsOut{iField}))
        sOut.(fieldsOut{iField}) = cast(round(...
          interp1 (sIn.(refChannel), cast(sIn.(fieldsOut{iField}), 'double'), sOut.(refChannel), interpMethod) ...
                                              ), 'logical');   
          warning(['Logical field ' fieldsOut{iField} ' was rounded during resampling (>0.5=false, >=0.5=true)'])
        else
          warning(['Could not convert field ' fieldsOut{iField}])
        end
      end
    end
  end %skip refChannel
end

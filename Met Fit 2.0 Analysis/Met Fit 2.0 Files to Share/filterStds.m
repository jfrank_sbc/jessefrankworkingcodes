[num, ~, raw] = xlsread('summary_filtered_data.xlsx', 'Std');
figure
%ss = struct2table(stage_std);
for i = 1:size(num,2)
    plot(num(:,i),'-o')
    hold on
end
legend

med = median(num,1);
mea = mean(num,1);

figure;
plot(s, med, '-o')
hold on
plot(s,mea, '-o')
set(gca,'xtick',[1:24],'xticklabel',xl)
xtickangle(45)
ylabel('Pmet Standard Deviation (W)')
title('Standard Deviation of Metabolic Power Across Filter Methods and Time Points')
legend('Medians','Means')
function [sandwiches, diffs, stage, mean_diff, aerobars,basebars, fname, std_diff] = FilteredAveraging_Final(data, fname);

%This script analyzes EEm data for each stage of the basebar washout
%protocol. There are 3 warmup stages (ignored during this analysis), and 16
%other stages (7 basebar, 6 aerobar). Each stage start is noted in the
%Marker Column of the K5 data file. The end of the test is also marked.
%This gives us 17 data points in stage_ends.

stage_ends = find(data.idMarker); % in excel file stages are noted by a number in an othewise empty column. Thus we search for the indices where there is a data entry
count = 0;

avg_count = count + 1;

% Loop through each stage. Analyze VO2 + EEm over the last 15 breath, ending at minute 4.
    for ii = 2 : length(stage_ends)
        tEnd =   data.t((stage_ends(ii)));
        
%         t2 = tEnd - 240;
%         t3 = tEnd - 180;
%         t4 = tEnd - 210;
%         [~, t2idx] = min(unique(round(abs(data.t- t2)),'stable')); 
%         [~, t3idx] = min(unique(round(abs(data.t- t3)),'stable'));
%         [~, t4idx] = min(unique(round(abs(data.t- t4)),'stable'));
          [~, t3idx] = min(unique(round(abs(data.t- tEnd)),'stable'));
        %means
        m34_mov = data.EEm_movmean(t3idx);
        m34_mov_VO2 = data.VO2_movmean(t3idx);
        

        %Standard Deviations
        s34_mov = data.EEm_movstd(t3idx);
        s34_mov_VO2 = data.VO2_movstd(t3idx);

        
        count = count + 1; %counting variable to assign a row in the EEm Data matrix. Each incrememnt represents one stage.
        
        %create a struct to hold the filtered and time point data for each
        %stage. 
        % Each row is 1 stage. 
        %Each column is a different time point using the same filtered method. 
        %Each field is a different filter methods.
        stage.EEm_movmean_60(count,1) = [m34_mov];
        stage.VO2_movmean_60(count,1) = [m34_mov_VO2];


        stage_std.EEm_movstd_60(count,1) = [s34_mov];
        stage_std.VO2_movstd_60(count,1) = [s34_mov_VO2];    
    end
  clear stage_ends   


%% Basebar Sandwich Analysis

%Split stage struct into just basebar trials and just aero trials. This
%will also act to remove the warmup trials from the data

basebars =  stage.EEm_movmean_60(4:2:end,1); %Take data from the 4th minute. last 15 breaths.
aerobars = stage.EEm_movmean_60(5:2:end,1);

base_std = stage_std.EEm_movstd_60(4:2:end,1)';
aero_std = stage_std.EEm_movstd_60(5:2:end,1)';

%create basebars sandwiches (average of the basebar trials directly before
%and after the aero stage). This will be used for Aero comparisons to the basebar sandwiches. (Diff array below).
for jj = 1: (length(basebars)-1)
    sandwiches(jj) = mean(basebars(jj:jj+1));
    diffs(jj) = aerobars(jj) - sandwiches(jj); 
    %sandwiches_std = mean(base_std(jj:jj+1));
    %diffs_std = 
end

% Each aero position was repated twice in the protocol. So in the diff
% array, each aero position has two data points. 
%Below we average these two datapoints into one number per aero positions.
for jj = 1:3
    mean_diff(jj) = mean([diffs(jj),diffs(length(diffs)-(jj-1))]);
    %mean_diff(jj) = mean([diffs(jj),diffs(length(diffs)-(jj-1))]);
    %mean_diff(jj) = mean([diffs(jj),diffs(length(diffs)-(jj-1))]);
    std_diff(jj) =  std([aero_std(jj),aero_std(length(aero_std)-(jj-1))]);
end
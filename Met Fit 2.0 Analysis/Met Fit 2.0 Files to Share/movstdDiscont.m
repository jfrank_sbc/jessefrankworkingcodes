function outS = movstdDiscont(signal, t, k)
%
% This function builds the moving average of signals that
% are samples with non-uniform sampling rate
% Modified movmeanDiscont code from Marcel Keyser by Jesse Frank
% Input
%  signal  - raw signal
%  t       - time vector, same length as signal
%  k       - input similar to native function movstd. Can be scalar or 2xn using [kb kf] format
%
% Output 
%  out     - filtered signal
%

tStep = [t(1); diff(t)];
signal_Weighted = signal .* tStep;

outS   = movstd(signal_Weighted, k) ./ movstd(tStep, k);

end %main function
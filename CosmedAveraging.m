fileNames = dir('*.xlsx'); %load all filenames in current directory. Make sure all data files are in the current directory
count = 0;
avg_count = 0;
for i = 1 : length(fileNames)
   [num txt raw] = xlsread(fileNames(i).name);
    stage_ends = find(isnan(num(:,8)) == 0); % in excel file stages are noted by a number in an othewise empty column. Thus we search for the indices where there is a data entry
    num(:,9) = single(num(:,9) * 86400); %convert time column back to seconds.Make single precision 
    avg_count = count + 1;
    for ii = 1 : length(stage_ends)
        tEnd =   num(stage_ends(ii),9);
        
%         % Find VO2 Average Min 2-3 of stage
%         t2 = tEnd - 120; %subtract 120 seconds from the end stage time. This gives us the start of minute 2.
%         t3 = tEnd - 60;  %%subtract 60 seconds from the end stage time. This gives us the start of minute 3.
%         [~, t2idx] = min(unique(round(abs(num(:,9)- t2)),'stable')); %determine the time value closest to t2 since bxb and get indice of value
%         [~, t3idx] = min(unique(round(abs(num(:,9)- t3)),'stable'));
%         m23 = mean(num(t2idx:t3idx,14)); %average VO2 data between t2 and t3 using the idices from above 
%         
%         %Find VO2 Average Min 2:30- 3:00 of stage
%         t2 = tEnd - 90;
%         t3 = tEnd - 60;
%         [~, t2idx] = min(unique(round(abs(num(:,9)- t2)),'stable'));
%         [~, t3idx] = min(unique(round(abs(num(:,9)- t3)),'stable'));
%         m233 = mean(num(t2idx:t3idx,14));
%         
%         %Find VO2 Average Min 3:00- 4:00 of stage
%         t2 = tEnd - 60;
%         [~, t2idx] = min(unique(round(abs(num(:,9)- t2)),'stable'));
%         m34 = mean(num(t2idx:stage_ends(ii),14));
        
        %Find VO2 Average Min 3:30- 4:00 of stage
        t2 = tEnd - 60;
        [~, t2idx] = min(unique(round(abs(num(:,9)- t2)),'stable'));
        m334 = mean(num(t2idx:stage_ends(ii),14));
        
        %Find PMet Avg Min 3:30 - 4:00 of stage
        p334 = (mean(num(t2idx:stage_ends(ii),44)))* 69.78; % 1 W = 0.0143 kcal/min
         %counting variable to assign a row in the VO2 Data matrix. Each incrememnt represents one stage.
         
         %Find Respiratory Frequency
         rf = (mean(num(t2idx:stage_ends(ii),10)));
         
         %Find Tidal Volume
         Vt = (mean(num(t2idx:stage_ends(ii),11)));
         
         %Find Minute Ventilation
         Ve = (mean(num(t2idx:stage_ends(ii),12)));
         
         %Find HR
         hr = (mean(num(t2idx:stage_ends(ii),23)));
       
        %Find RER Avg Min 3:30-4
        r334 = (mean(num(t2idx:stage_ends(ii),16))); % 1 W = 0.0143 kcal/min
        count = count + 1; %counting variable to assign a row in the VO2 Data matrix. Each incrememnt represents one stage.
       
        
        %VO2_Data(count,1:4) = [m23 m233 m34 m334];
        VO2_Data(count,1) = [m334];
        Avg_Sbj_Data(i, 1) = mean(VO2_Data(avg_count: (avg_count + ii - 1),1)); 
        
        Pmet_Data(count,1) = p334;
        Avg_Sbj_Pmet(i,1) = mean(Pmet_Data(avg_count: (avg_count + ii - 1),1));
        
        RERmet_Data(count,1) = r334;
        Avg_Sbj_RER(i,1) = mean(RERmet_Data(avg_count: (avg_count + ii - 1),1));
        
        rf_Data(count,1) = [rf];
        
        Ve_Data(count,1) = [Ve];
        
        Vt_Data(count,1) = [Vt];
        
        HR_Data(count,1) = [hr];
        

     end
  clear stage_ends   
  
  
end

summary_data = [Pmet_Data RERmet_Data rf_Data Ve_Data VO2_Data Vt_Data HR_Data];
% outside_res = sum(pd(:,:) ==1); %count how many values in each percent diff column are greater than 2%
% 
% for j = 1 : length(Avg_Sbj_Data)
%     hold on
%     plot(Avg_Sbj_Data(j,1:4),'-o')
% end
% 
% figure
% for j = 1 : length(VO2_Data)
%     hold on
%     plot(VO2_Data(j,1:4),'-o')
% end
% figure
% for j = 1 : length(pdValues)
%     hold on
%     plot(pdValues(j,1:3),'-o')
% end
function [stage, data, stage_std ] = metFit3_blockProtocol_analysis(power1, power2, power3);
clear stage_ends
clear stage
clear stage_std

%User selects a file
%remember previous path
fpathMemoryFile = fullfile(fileparts(mfilename('fullpath')), 'fpathPrevious.mat');
if exist(fpathMemoryFile, 'file')
  load(fpathMemoryFile); %#ok<LOAD>
else
  fpath = '';
end
[fname, fpath] = uigetfile(fullfile(fpath, '*.xlsx'),'MultiSelect','on');
%fWait = waitbar(0.3,'Loading Excel file...');

for i = 1 : length(fname)
    [~,~,raw] = xlsread(fullfile(fpath,fname)); %read in each selected file, 1 by 1

    %%
    data.t      = 24*3600*[raw{4:end,10}]';
    data.VO2    = [raw{4:end,15}]';
    data.EEm    = [raw{4:end,45}]'*69.78;
    data.HR     = [raw{4:end, 24}]';
    data.RER    = [raw{4:end,17}]';

    %extract marker
    Marker = raw(4:end,37);
    testStartID = find(cellfun(@ischar, Marker));
    testTitle   = Marker(testStartID);
    testTime    = data.t(testStartID);

    % Resample to unique step size
    fSampling = 1; %[Hz]

    %leading signal Time must be unique
    [data.t, uniqueIDs] = unique(data.t);
    data.VO2 = data.VO2(uniqueIDs);
    data.EEm = data.EEm(uniqueIDs); %All .EEm variables/structs were added by Jesse so we can have VO2 and EEm data ready to go.
    data.HR = data.HR(uniqueIDs);
    data.RER = data.RER(uniqueIDs);



    %data.t = round(data.t, 1); %otherwise "next" method cause delay on one sample

    %% apply weighted Mov Mean filter

    % calculate smoothed signal
    %Marcel's initial code looking at previous 14 breaths means
    nFilt = 15; %[amount of breath] to average
    data.VO2_movmean         = movmean(data.VO2, [nFilt-1 0]);
    data.EEm_movmean         = movmean(data.EEm, [nFilt-1 0]);
    data.HR_movmean         = movmean(data.HR, [nFilt-1 0]);
    data.RER_movmean         = movmean(data.RER, [nFilt-1 0]);

    %Std
    data.VO2_movstd         = movstd(data.VO2, [nFilt-1 0]);
    data.EEm_movstd         = movstd(data.EEm, [nFilt-1 0]);
    data.HR_movstd         = movstd (data.HR, [nFilt-1 0]);
    data.RER_movstd         = movstd (data.RER, [nFilt-1 0]);


    %add a trigger signal, where a new test was starting
    data.idMarker = false(size(data.t));
    data.idMarker( arrayfun(@(X) find(data.t>=X, 1, 'first'), testTime) ) = true;


    stage_ends = find(data.idMarker); % in excel file stages are noted by a number in an othewise empty column. Thus we search for the indices where there is a data entry
    count = 0;
    gCount = 0;
    avg_count = count + 1;
    colmn = 1;

    % Loop through each stage. Analyze VO2 + EEm over the last 15 breath, ending at minute 4.
    for ii = 2 : length(stage_ends)
        tEnd =   data.t((stage_ends(ii)));

        [~, t3idx] = min(unique(round(abs(data.t- tEnd)),'stable'));
        %means
        EEm_mov = data.EEm_movmean(t3idx);
        VO2_mov = data.VO2_movmean(t3idx);
        HR_mov = data.HR_movmean(t3idx);
        RER_mov = data.RER_movmean(t3idx);




        %Standard Deviations
        EEm_mov_std = data.EEm_movstd(t3idx);
        VO2_mov_std = data.VO2_movstd(t3idx);
        HR_mov_std = data.HR_movstd(t3idx);
        RER_mov_std = data.RER_movstd(t3idx);


        count = count + 1; %counting variable to assign a row in the EEm Data matrix. Each incrememnt represents one stage.

        %create a struct to hold the filtered and time point data for each
        %stage.
        % Each row is 1 stage.
        %Each column is a different time point using the same filtered method.
        %Each field is a different filter methods.
        gCount = gCount + 1;
        if gCount == 5 %only four condtions. Have it reset after 4.
            gCount = 1;
            colmn = colmn + 1;
        end

        stage.EEm_movmean_60(gCount,colmn) = [EEm_mov];
        stage.VO2_movmean_60(gCount,colmn) = [VO2_mov];
        stage.HR_movmean_60(gCount,colmn) = [HR_mov];
        stage.RER_movmean_60(gCount,colmn) = [RER_mov];

        % calcualte GME



        if gCount == 1
            stage.GME(gCount,colmn) = power1/EEm_mov;
        elseif gCount == 2 || gCount == 4
            stage.GME(gCount,colmn) = power2/EEm_mov;
        elseif gCount == 3
            stage.GME(gCount,colmn) = power3/EEm_mov;
        end




        stage_std.EEm_movstd_60(gCount,colmn) = [EEm_mov_std];
        stage_std.VO2_movstd_60(gCount,colmn) = [VO2_mov_std];
        stage_std.HR_movstd_60(gCount,colmn) = [HR_mov_std];
        stage_std.RER_movstd_60(gCount,colmn) = [RER_mov_std];
    end
end



    %% Read in CORE data
    cc = inputdlg('Was CORE data collected? If yes enter Y, if no enter N');
    comp = inputdlg('What type of cycling computer was used? If Wahoo enter W, if Garmin enter G');

    if strcmp(cc, 'Y') == 1 || strcmp(cc,'y') == 1
        
        %%Read in CORE data from .fit file
        [fname, fpath] = uigetfile(fullfile(fpath, '*.xlsx'),'MultiSelect','on');
        [~, ~, cRaw] = xlsread(fullfile(fpath,fname));
        %[~, ~, cRaw] = xlsread('C:\Users\Jesse Frank\OneDrive - Specialized Bicycle Components\Projects\Met Fit 3.0\Initial Pilot Tests\220620_VELO\Luke\220614_JF_OutdoorsPilot_Lefthand_Garmin.xlsx');
        if strcmp(comp, 'G') == 1 || strcmp(comp,'g') == 1
            data.core = [cRaw{2:end,22}]';
        elseif strcmp(comp, 'W') == 1 || strcmp(cc,'w') == 1
            data.core = [cRaw{2:end,21}]';
        else
            error('Wrong cycling computer type entered')
        end
        data.core = resample(data.core,length(data.t),length(data.core));

        %data.core = data.core(uniqueIDs);
        data.core_movmean         = movmean(data.core, [nFilt-1 0]);
        data.core_movstd         = movstd(data.core, [nFilt-1 0]);

        %resetting counting variables from above
        count = 0;
        gCount = 0;
        avg_count = count + 1;
        colmn = 1;

        % Loop through each stage. Analyze CORE over the last 15 breath, ending at minute 4.
        for ii = 2 : length(stage_ends) %
            tEnd =   data.t((stage_ends(ii)));

            [~, t3idx] = min(unique(round(abs(data.t- tEnd)),'stable'));
            %means
            core_mov = data.core_movmean(t3idx);

            %Standard Deviations
            core_mov_std = data.EEm_movstd(t3idx);

            count = count + 1; %counting variable to assign a row in the EEm Data matrix. Each incrememnt represents one stage.

            %create a struct to hold the filtered and time point data for each
            %stage.
            % Each row is 1 stage.
            %Each column is a different time point using the same filtered method.
            %Each field is a different filter methods.
            gCount = gCount + 1;
            if gCount == 5 %only four condtions. Have it reset after 4.
                gCount = 1;
                colmn = colmn + 1;
            end

            stage.core_movmean_60(gCount,colmn) = [core_mov];

            stage_std.core_movstd_60(gCount,colmn) = [core_mov_std];
        end

%         subplot(2,3,6)
%         plot(stage.core_movmean_60,'-o')
%         title('CORE Temp')
%         xlabel('Mechanical Power Output (W)')
%         ylabel('Core Temp (C)')
%         xticks([1 2 3 4])
%         set(gca, 'xticklabels',{power1 power2 power3 power2});
    else
    end
%% Assign to plotting structure

%% Plot

figure
    subplot(2,3,1)
    plot(stage.VO2_movmean_60,'-o')
    title('VO2')
    xlabel('Mechanical Power Output (W)')
    ylabel('mL O2/min')
    xticks([1 2 3 4])
    set(gca, 'xticklabels',{power1 power2 power3 power2});
   


    subplot(2,3,2)
    plot(stage.HR_movmean_60,'-o')
    title('Heart Rate')
    xlabel('Mechanical Power Output (W)')
    ylabel('bpm')
    xticks([1 2 3 4])
    set(gca, 'xticklabels',{power1 power2 power3 power2});


    subplot(2,3,3)
    plot(stage.GME .* 100,'-o')
    title('Gross Mechanical Efficiency')
    xlabel('Mechanical Power Output (W)')
    ylabel('%')
    xticks([1 2 3 4])
    set(gca, 'xticklabels',{power1 power2 power3 power2});


    subplot(2,3,4)
    plot(stage.EEm_movmean_60,'-o')
    title('Metabolic Power')
    xlabel('Mechanical Power Output (W)')
    ylabel('Metabolic Power (W) ')
    xticks([1 2 3 4])
    set(gca, 'xticklabels',{power1 power2 power3 power2});


    subplot(2,3,5)
    plot(stage.RER_movmean_60,'-o')
    title('Respiratory Exchange Ratio')
    xlabel('Mechanical Power Output (W)')
    ylabel('RER')
    xticks([1 2 3 4])
    set(gca, 'xticklabels',{power1 power2 power3 power2});

    subplot(2,3,6)
    plot(stage.core_movmean_60,'-o')
    title('CORE Temp')
    xlabel('Mechanical Power Output (W)')
    ylabel('Core Temp (C)')
    xticks([1 2 3 4])
    set(gca, 'xticklabels',{power1 power2 power3 power2});

    legend('baseline', '-2 cm','Location','bestoutside')
    lgn = legend('show');
    lgn.Position(1) = 0.0001;
    lgn.Position(2) = 0.94;

    
end





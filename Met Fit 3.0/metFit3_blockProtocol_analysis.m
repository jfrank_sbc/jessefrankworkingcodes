function [stage, data, stage_std, position ] = metFit3_blockProtocol_analysis();
clear stage_ends
clear stage
clear stage_std

%Enter Power that the rider held. Need power number for each trial,
        %as the rider may not have exactly hit the number as expected.
        % each power variable is one trial run, with the first number being
        % the first 5 min power and the last number being the last 5 min
        % power

        %power1 = [270,344,377,340]'; %Vlasov
        %power2 = [292,350,373,331]'; %Vlasov
        %power1 = [273,338,383,352]'; %Cian
        %power2 = [291,345,374,348]'; %Cian
        %power1 = [283,346,380,328]'; %Max S.TT
        %power2 = [276,338,370,316]'; %Max S. Road
        %power1 = [292,372,411,373]'; %Nils 5 min avg pwer
        %power2 = [298,372,412,378]'; %Nils 5 min avg pwer
        %power1 = [306,386,410,386]'; %Nils 15 breath avg pwer
        %power2 = [305,372,416,383]'; %Nils 15 breath avg pwer
       
%Get test wattage from test 1
        power1 = inputdlg(['Enter the wattage from each stage of test 1, separated by a comma.' sprintf('\n') 'NOTE: These values should be the actual value the rider held, not what they were instructed to hold']);
        power1 = (str2num(cell2mat(power1)))';

%Get test wattage from test 2
        power2 = inputdlg(['Enter the wattage from each stage of test 2, separated by a comma.' sprintf('\n') 'NOTE: These values should be the actual value the rider held, not what they were instructed to hold']);
        power2 = (str2num(cell2mat(power2)))';

        powers = [power1 power2];% power3];
       

%User selects a file
%remember previous path
fpathMemoryFile = fullfile(fileparts(mfilename('fullpath')), 'fpathPrevious.mat');
if exist(fpathMemoryFile, 'file')
  load(fpathMemoryFile); %#ok<LOAD>
else
  fpath = '';
end
[fname, fpath] = uigetfile(fullfile(fpath, '*.xlsx'),'MultiSelect','on');
%fWait = waitbar(0.3,'Loading Excel file...');

for i = 1 : length(fname)
    [~,~,raw] = xlsread(fullfile(fpath,fname{i})); %read in each selected file, 1 by 1

    %%
    data.t      = 24*3600*[raw{4:end,10}]';
    data.VO2    = [raw{4:end,15}]';
    data.EEm    = [raw{4:end,45}]'*69.78;
    data.HR     = [raw{4:end,24}]';
    data.RER    = [raw{4:end,17}]';

    %extract marker
    Marker = raw(4:end,37);
    testStartID = find(cellfun(@ischar, Marker));
    testTitle   = Marker(testStartID);
    testTime    = data.t(testStartID);

    % Resample to unique step size
    fSampling = 1; %[Hz]

    %leading signal Time must be unique
    [data.t, uniqueIDs] = unique(data.t);
    data.VO2 = data.VO2(uniqueIDs);
    data.EEm = data.EEm(uniqueIDs); %All .EEm variables/structs were added by Jesse so we can have VO2 and EEm data ready to go.
    data.HR = data.HR(uniqueIDs);
    data.RER = data.RER(uniqueIDs);
    %data.t = round(data.t, 1); %otherwise "next" method cause delay on one sample

    %% apply weighted Mov Mean filter

    % calculate smoothed signal
    %Marcel's initial code looking at previous 14 breaths means
    nFilt = 15; %[amount of breath] to average
    data.VO2_movmean         = movmean(data.VO2, [nFilt-1 0],'omitnan');
    data.EEm_movmean         = movmean(data.EEm, [nFilt-1 0],'omitnan');
    data.HR_movmean         = movmean(data.HR, [nFilt-1 0],'omitnan');
    data.RER_movmean         = movmean(data.RER, [nFilt-1 0],'omitnan');

    %Std
    data.VO2_movstd         = movstd(data.VO2, [nFilt-1 0],'omitnan');
    data.EEm_movstd         = movstd(data.EEm, [nFilt-1 0],'omitnan');
    data.HR_movstd         = movstd (data.HR, [nFilt-1 0],'omitnan');
    data.RER_movstd         = movstd (data.RER, [nFilt-1 0],'omitnan');


    %add a trigger signal, where a new test was starting
    data.idMarker = false(size(data.t));
    data.idMarker( arrayfun(@(X) find(data.t>=X, 1, 'first'), testTime) ) = true;


    stage_ends = find(data.idMarker); % in excel file stages are noted by a number in an othewise empty column. Thus we search for the indices where there is a data entry
    count = 0;
    gCount = 0;
    avg_count = count + 1;
    colmn = 1;

    % Loop through each stage. Analyze VO2 + EEm over the last 15 breath, ending at minute 4.
    for ii = 2 : length(stage_ends)
        tEnd =   data.t((stage_ends(ii)));

        [~, t3idx] = min(unique(round(abs(data.t- tEnd)),'stable'));
        %means
        EEm_mov = data.EEm_movmean(t3idx);
        VO2_mov = data.VO2_movmean(t3idx);
        HR_mov = data.HR_movmean(t3idx);
        RER_mov = data.RER_movmean(t3idx);




        %Standard Deviations
        EEm_mov_std = data.EEm_movstd(t3idx);
        VO2_mov_std = data.VO2_movstd(t3idx);
        HR_mov_std = data.HR_movstd(t3idx);
        RER_mov_std = data.RER_movstd(t3idx);


        count = count + 1; %counting variable to assign a row in the EEm Data matrix. Each incrememnt represents one stage.

        %create a struct to hold the filtered and time point data for each
        %stage.
        % Each row is 1 stage.
        %Each column is a different time point using the same filtered method.
        %Each field is a different filter methods.
        gCount = gCount + 1;
        if gCount == 5 %only four condtions. Have it reset after 4.
            gCount = 1;
            colmn = colmn + 1;
        end

        stage.EEm_movmean_60(gCount,colmn) = [EEm_mov];
        stage.VO2_movmean_60(gCount,colmn) = [VO2_mov];
        stage.HR_movmean_60(gCount,colmn) = [HR_mov];
        stage.RER_movmean_60(gCount,colmn) = [RER_mov];
        %stage.Relative_VO2_Power(gcount,colmn) = [VO2_mov./] %Add Relative
        %VO2/Watt, as requested by Dan Lorang

        % calcualte GME

        if gCount == 1
            stage.GME(gCount,colmn) = powers(gCount,i)./EEm_mov;
            stage.pdiffGME(gCount,colmn) = 0;
        elseif gCount == 2 %|| gCount == 4
            stage.GME(gCount,colmn) = powers(gCount,i)./EEm_mov;
            stage.pdiffGME(gCount,colmn) = (stage.GME(gCount,colmn)-stage.GME(1,colmn))/stage.GME(1,colmn);
        elseif gCount == 3
            stage.GME(gCount,colmn) = powers(gCount,i)./EEm_mov;
            stage.pdiffGME(gCount,colmn) = (stage.GME(gCount,colmn)-stage.GME(1,colmn))/stage.GME(1,colmn);
        elseif gCount == 4
            stage.GME(gCount,colmn) = powers(gCount,i)./EEm_mov;
            stage.pdiffGME(gCount,colmn) = (stage.GME(gCount,colmn)-stage.GME(1,colmn))/stage.GME(1,colmn);
        end

        %Assign variables for standard deviation
        stage_std.EEm_movstd_60(gCount,colmn) = [EEm_mov_std];
        stage_std.VO2_movstd_60(gCount,colmn) = [VO2_mov_std];
        stage_std.HR_movstd_60(gCount,colmn) = [HR_mov_std];
        stage_std.RER_movstd_60(gCount,colmn) = [RER_mov_std];
    end


    if i == 1
        position.baseline = stage;
    elseif i == 2
        position.minus = stage;
    elseif i ==3
        position.plus = stage;
    else
    end
end

%% Calculate Relative Effort quantified by VO2/Stage Power
%Requested by Dan Lorang
position.baseline.relEff = position.baseline.VO2_movmean_60./power1;
position.minus.relEff = position.minus.VO2_movmean_60./power2;

%% Calcualte GME Diff

position.GMEdiff= (((position.baseline.GME)- (position.minus.GME))./(position.minus.GME))*100;
position.MechPdiff = (((powers(:,1))- (powers(:,2))./(powers(:,2))))*100;

%% call plotting function

plotMetFit3(position,powers)   

end
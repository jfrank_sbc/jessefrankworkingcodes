function plotMetFit3(data,powers)

figure
subplot(3,3,1)
plot(data.baseline.VO2_movmean_60,'-o')
hold on
%plot(data.plus.VO2_movmean_60,'-o')
plot(data.minus.VO2_movmean_60,'-o')
title('VO2')
ylabel('mL O2/min')
grid minor
xticks([1 2 3 4])
set(gca, 'xticklabels',{'stage1' 'stage2' 'stage3' 'stage4'});



subplot(3,3,2)
plot(data.baseline.HR_movmean_60,'-o')
hold on
%plot(data.plus.HR_movmean_60,'-o')
plot(data.minus.HR_movmean_60,'-o')
title('Heart Rate')
ylabel('bpm')
xticks([1 2 3 4])
grid minor
set(gca, 'xticklabels',{'stage1', 'stage2', 'stage3', 'stage4'});


subplot(3,3,3)
plot(data.baseline.GME .* 100,'-o')
hold on
%plot(data.plus.GME.*100,'-o')
plot(data.minus.GME.*100,'-o')
title('Gross Mechanical Efficiency')
ylabel('%')
xticks([1 2 3 4])
grid minor
set(gca, 'xticklabels',{'stage1', 'stage2', 'stage3', 'stage4'});


subplot(3,3,4)
plot(data.baseline.EEm_movmean_60,'-o')
hold on
%plot(data.plus.EEm_movmean_60,'-o')
plot(data.minus.EEm_movmean_60,'-o')
title('Metabolic Power')
ylabel('Metabolic Power (W) ')
xticks([1 2 3 4])
grid minor
set(gca, 'xticklabels',{'stage1', 'stage2', 'stage3', 'stage4'});


subplot(3,3,5)
plot(data.baseline.RER_movmean_60,'-o')
hold on
%plot(data.plus.RER_movmean_60,'-o')
plot(data.minus.RER_movmean_60,'-o')
title('Respiratory Exchange Ratio')
ylabel('RER')
xticks([1 2 3 4])
grid minor
set(gca, 'xticklabels',{'stage1', 'stage2', 'stage3', 'stage4'});

subplot(3,3,6)
plot(data.GMEdiff, 'ko-')
hold on
plot(data.MechPdiff, 'go-')
title('Percent Diff GME: Start to End of Day')
ylabel('Percent Diff of GME (%)')
xticks([1 2 3 4])
grid minor
set(gca, 'xticklabels',{'stage1', 'stage2', 'stage3', 'stage4'});
ylim([-12 12])

% subplot(3,3,6)
% plot(data.baseline.pdiffGME(2:end).*100,'-o')
% hold on
% %plot(data.plus.pdiffGME(2:end).*100,'-o')
% plot(data.minus.pdiffGME(2:end).*100,'-o')
% title('Percent Difference in GME from Stage 1')
% ylabel('Percent Difference')
% xticks([1 2 3 4])
% grid minor
% set(gca, 'xticklabels',{'stage2', 'stage3', 'stage4'});

subplot(3,3,8)
plot(powers(:,1),'-o')
hold on
plot(powers(:,2),'-o')
%plot(powers(:,3),'-o')
title('Mechanical Powers for Each Stage')
ylabel('Mech Power (W)')
xticks([1 2 3 4])
grid minor
set(gca, 'xticklabels',{'stage1', 'stage2', 'stage3', 'stage4'});

subplot(3,3,9)
plot(data.baseline.relEff,'-o')
hold on
plot(data.minus.relEff,'-o')
title('VO2/W')
ylabel('VO2/W (mL O2/min*W')
xticks([1 2 3 4])
grid minor
set(gca, 'xticklabels',{'stage1', 'stage2', 'stage3', 'stage4'});

%legend('baseline','+2 cm', '-2 cm','Location','bestoutside')
legend('baseline','final','Location','bestoutside')
%legend('TT','Road','Location','bestoutside')
lgn = legend('show');
lgn.Position(1) = 0.1;
lgn.Position(2) = 0.15;

end
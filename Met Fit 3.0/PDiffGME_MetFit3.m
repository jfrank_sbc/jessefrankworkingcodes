GMEdiff= (((position.baseline.GME)- (position.minus.GME))./(position.minus.GME))*100
MechPdiff = (((power.TT)- (power.Road))./(power.Road))*100
figure; plot(GMEdiff, 'ko-')
hold on
plot(MechPdiff, 'go-')
title('Percent Diff GME: Start to End of Day')
ylabel('Percent Diff of GME (%)')
xticks([1 2 3 4])
grid minor
set(gca, 'xticklabels',{'stage1', 'stage2', 'stage3', 'stage4'});
ylim([-12 12])

%%Figure out how to incorporate into main metf3_blockprotocol script
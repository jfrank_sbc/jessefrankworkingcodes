# -*- coding: utf-8 -*-
"""
Created on Mon Jun  1 17:31:44 2020

@author: jfrank
"""


import pandas as pd
import numpy as np
import matplotlib.pyplot as plt
import seaborn as sns
import os


get_ipython().run_line_magic('matplotlib', 'inline')

sns.set_style('whitegrid')


df = pd.read_excel(r"C:\Users\jfrank\OneDrive - Specialized Bicycle Components\Projects\Thermal\Thermal Future Project\Men\2020Jan28\Jan28Sum.xlsx")
df.info()

tsuda =df.loc[df['Name'] == 'Tsuda']
koesel = df.loc[df['Name'] == 'Koesel']
bibko = df.loc[df['Name'] == 'Bibko']
fischer = df.loc[df['Name'] == 'Fischer']
quay = df.loc[df['Name'] == 'Quay']

tb = bibko['Min']

## Power
e = tsuda['Pwr Std']
plt.errorbar(tb,tsuda['Pwr'],yerr=e,fmt='o-')
e = fischer['Pwr Std']
plt.errorbar(tb,fischer['Pwr'],yerr=e,fmt='o-')
e = koesel['Pwr Std']
plt.errorbar(tb,koesel['Pwr'],yerr=e,fmt='o-')
e = bibko['Pwr Std']
plt.errorbar(tb,bibko['Pwr'],yerr=e,fmt='o-')
#e = quay['Pwr Std']
#plt.errorbar(tb,quay['Pwr'],yerr=e,fmt='o-')

plt.legend(["tsuda","fischer","koesel","bibko"],loc = 'lower center')
#plt.legend(["tsuda","koesel","bibko","quay"],loc = 'lower center')
#plt.legend(["tsuda","fischer","bibko","quay"],loc = 'lower center')
#plt.legend(["tsuda","koesel","bibko","quay"],loc = 'lower center')
plt.xlabel("Time (min)")
plt.ylabel("Power (W)")

plt.show()


## HR
e = tsuda['HR Std']
plt.errorbar(tb,tsuda['HR'],yerr=e,fmt='o-')
e = fischer['HR Std']
plt.errorbar(tb,fischer['HR'],yerr=e,fmt='o-')
e = koesel['HR Std']
plt.errorbar(tb,koesel['HR'],yerr=e,fmt='o-')
e = bibko['HR Std']
plt.errorbar(tb,bibko['HR'],yerr=e,fmt='o-')
#e = quay['HR Std']
#plt.errorbar(tb,quay['HR'],yerr=e,fmt='o-')

plt.legend(["tsuda","fischer","koesel","bibko"],loc = 'lower center')
#plt.legend(["fischer","koesel","bibko","quay"],loc = 'lower center')
#plt.legend(["tsuda","fischer","bibko","quay"],loc = 'lower center')
#plt.legend(["tsuda","koesel","bibko","fischer","quay"],loc = 'lower center')
#plt.legend(["tsuda","koesel","bibko","quay"],loc = 'lower center')
plt.xlabel("Time (min)")
plt.ylabel("Heart Rate (bpm)")

plt.show()
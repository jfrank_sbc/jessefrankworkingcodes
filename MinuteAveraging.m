fileNames = dir('*.xlsx'); %load all filenames in current directory. Make sure all data files are in the current directory
count = 0;
avg_count = 0;
for i = 1 : length(fileNames)
   [num txt raw] = xlsread(fileNames(i).name);
    stage_ends = find(isnan(num(:,8)) == 0); % in excel file stages are noted by a number in an othewise empty column. Thus we search for the indices where there is a data entry
    num(:,9) = single(num(:,9) * 86400); %convert time column back to seconds.Make single precision 
    avg_count = count + 1;
    for ii = 1 : length(stage_ends)
        tEnd =   num(stage_ends(ii),9);
        
        % Find VO2 Average Min 2-3 of stage
        t2 = tEnd - 120; %subtract 120 seconds from the end stage time. This gives us the start of minute 2.
        t3 = tEnd - 60;  %%subtract 60 seconds from the end stage time. This gives us the start of minute 3.
        [~, t2idx] = min(unique(round(abs(num(:,9)- t2)),'stable')); %determine the time value closest to t2 since bxb and get indice of value
        [~, t3idx] = min(unique(round(abs(num(:,9)- t3)),'stable'));
        m23 = mean(num(t2idx:t3idx,14)); %average VO2 data between t2 and t3 using the idices from above 
        
        %Find VO2 Average Min 2:30- 3:00 of stage
        t2 = tEnd - 90;
        t3 = tEnd - 60;
        [~, t2idx] = min(unique(round(abs(num(:,9)- t2)),'stable'));
        [~, t3idx] = min(unique(round(abs(num(:,9)- t3)),'stable'));
        m233 = mean(num(t2idx:t3idx,14));
        
        %Find VO2 Average Min 3:00- 4:00 of stage
        t2 = tEnd - 60;
        [~, t2idx] = min(unique(round(abs(num(:,9)- t2)),'stable'));
        m34 = mean(num(t2idx:stage_ends(ii),14));
        
        %Find VO2 Average Min 3:30- 4:00 of stage
        t2 = tEnd - 30;
        [~, t2idx] = min(unique(round(abs(num(:,9)- t2)),'stable'));
        m334 = mean(num(t2idx:stage_ends(ii),14));
        
        count = count + 1; %counting variable to assign a row in the VO2 Data matrix. Each incrememnt represents one stage.
        VO2_Data(count,1:4) = [m23 m233 m34 m334];
        Avg_Sbj_Data(i, 1:4) = mean(VO2_Data(avg_count: (avg_count + ii - 1),1:4)); 
        
        % Percent Diff compared to Min 3:30 - 4
        pd1 = 100 * (m23 - m334)./m334;
        pd2 = 100 * (m233 - m334)./m334;
        pd3 = 100 * (m34 - m334)./m334;
        
        pdValues(count, 1:3) = [pd1 pd2 pd3];
        
       % If percentage differences between time points is greater than 2%
       % (k5 resolution)
       % put a '1' in teh cell. If not, put a 0
        if abs(pd1) > 2
            pd(count, 1) = 1;
        else
            pd(count, 1) = 0;
        end
        
        if abs(pd2) > 2
            pd(count, 2) = 1;
        else
            pd(count, 2) = 0;
        end
        
        if abs(pd3) > 2
            pd(count, 3) = 1;
        else
            pd(count, 3) = 0;
        end
    end
  clear stage_ends   
  
  
end

outside_res = sum(pd(:,:) ==1); %count how many values in each percent diff column are greater than 2%

for j = 1 : length(Avg_Sbj_Data)
    hold on
    plot(Avg_Sbj_Data(j,1:4),'-o')
end

figure
for j = 1 : length(VO2_Data)
    hold on
    plot(VO2_Data(j,1:4),'-o')
end
figure
for j = 1 : length(pdValues)
    hold on
    plot(pdValues(j,1:3),'-o')
end
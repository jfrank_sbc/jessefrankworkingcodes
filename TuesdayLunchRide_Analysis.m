path1 = cd;
[files path] = uigetfile('*.*','MultiSelect','on'); %Opens dialog box to choose file
cd(path) %set current directory to file location


%% Column numbers for relevant metrics when .fit file is exported to csv
% secs - 1
% hr - 3
% pwr - 7
% temp - 13

%% Determine indices 
% Determine how long warmup was to skip ahead in excel sheet to when the
%wu = str2double(inputdlg('How long (in minutes) was the warmup?'));
start_num = 10 * 60; %determine start indices for analysis assuming 10 minutes of warmup and 1hz data collection

%determine how long effort was to determine indice length of effort
%effort = str2double(inputdlg('How long (in minutes) was the effort?'));
effort = 20;
end_num = 20 * 60; %determine end indices for analysis assuming 20 minutes of warmup and 1hz data collection
spacing = effort/5;

%% create time array for graph
for ii = 1: spacing
    t(ii) = 5*ii;
end
t = t';


%% Determine metrics over the last 30 seconds of each 5 minute block for the length of the effort and then plot the data 
for i = 1 : length(files)
   cd(path)
   [num txt raw] = xlsread(files{i}); %read in excel files
   cd(path1) %reset directory to location of analysis file
   count = 1;
%    for jj = (start_num+300): 300:(start_num+ end_num) %avg over last 30 seconds of each 5 min block
%        avg_power(count,i) = mean(num(jj-30:jj,7));
%        avg_hr(count,i) = mean(num(jj-30:jj,3));
%        avg_temp(count,i) = mean(num(jj-30:jj,13));
%        t_array(:,i) = t;
%        count = count + 1;
%    end
   for jj = (start_num + 300): 300:(start_num+ end_num) %avg over entire 5 min block
       avg_power(count,i) = mean(num(jj-300:jj,7));
       avg_hr(count,i) = mean(num(jj-300:jj,3));
       avg_temp(count,i) = mean(num(jj-300:jj,13));
       t_array(:,i) = t;
       count = count + 1;
   end
   figure(1)
   %plot(t_array(:,i),avg_power,'*')
   plot(t_array(:,i),avg_power(:,i),'-*')
   hold on
   figure(2)
   plot(t_array(:,i),avg_hr(:,i),'-o')
   hold on
   figure(3)
   plot(avg_hr(:,i),avg_power(:,i),'-o')
   hold on
end

%% Plot labels 
figure(1)
title('Power vs time')
xlabel('time (minutes)')
ylabel('Power (W)')
legend('Bibko','Fischer','Koesel','location','eastoutside')

figure(2)
title('HR vs time')
xlabel('time (minutes)')
ylabel('HR (bpm)')
legend('Bibko','Fischer','Koesel','location','eastoutside')


figure(3)
title('PWR vs HR')
xlabel('HR (bpm)')
ylabel('Power (Watts)')
legend('Bibko','Fischer','Koesel','location','eastoutside')

%% save data to excel sheet
[ExcelFile] = uigetfile('*.*');

sheet = 'Power 5';
xlRange = 'AG3';
xlswrite(ExcelFile,avg_power,sheet,xlRange)

sheet = 'HR 5';
xlRange = 'AG3';
xlswrite(ExcelFile,avg_hr,sheet,xlRange)

sheet = 'Ambient Temp';
xlRange = 'AG3';
xlswrite(ExcelFile,avg_temp,sheet,xlRange)

%% Save plots to folder for the date of the specific test
cd(path)
saveas(figure(1),'PowerVsTime5.fig')
saveas(figure(1),'PowerVsTime5.png')
saveas(figure(2),'HRVsTime5.fig')
saveas(figure(2),'HRVsTime5.png')
